var express = require('express');
var router = express.Router();
var cleweezevent = require('../config/api-weezevent');
const request = require('request');

router.get('/', function(req, res, next) {
	res.render('billetterie-soldout');
});

router.get('/stats', function (req, res, next) {

	//res.status(500).end();

	request(cleweezevent.cle, (err, resr, body) => {
		if (err) {
			res.status(500);
			res.send({
				"error": err
			});
		}else if(JSON.parse(resr.body).events != undefined){
			var result = JSON.parse(resr.body);

			var all = result.events[0].categories;
			var ticket = result.events[0].tickets;

			var vendredi = 0;
			var samedi = 0;
			var deuxsoirs = 0;

			all.forEach(function(cat) {
				cat.tickets.forEach(function(one) {
					if(one.name.indexOf("Vendredi")  != -1){
						vendredi += one.participants;
					}
					if(one.name.indexOf("Samedi") != -1){
						samedi += one.participants;
					}
					if(one.name.indexOf("2 nuits")  != -1){
						deuxsoirs += one.participants;
					}
				});
			});
			ticket.forEach(function(one) {
				if(one.name.indexOf("Vendredi")  != -1){
					vendredi += one.participants;
				}
				if(one.name.indexOf("Samedi") != -1){
					samedi += one.participants;
				}
				if(one.name.indexOf("2 nuits")  != -1){
					deuxsoirs += one.participants;
				}
			});
			ticket.forEach(function(one) {
				if(one.name.indexOf("Invitation vendredi")  != -1){
					vendredi -= one.participants;
				}
				if(one.name.indexOf("Invitation samedi") != -1){
					samedi -= one.participants;
				}
			});

			var vendrediTOT = Math.round((vendredi+5760)/10000*100);
			var samediTOT = Math.round((samedi+5760)/10000*100);
			var deuxsoirsTOT = Math.round((deuxsoirs)/5760*100);

			var deadline = new Date(2019, 05, 11,00,00,01);
			var debut = new Date(2019,02,03,14,00,00);
			var now = Date.now();
			var totaldate = deadline - debut;
			var restantdate = deadline - now;

			var pourcentagedate = Math.round(restantdate/totaldate*100);
			var placesvendu = vendredi+samedi+deuxsoirs*2;
			var pourcentageplace = Math.round(placesvendu/18000*100);


			console.log(vendredi+'--'+samedi+'--'+deuxsoirs);
			//console.log((samedi+5713) + '---' + (vendredi+5713));

			res.send({
				"vendredi": ((vendredi+5760)/10000*100).toFixed(1),
				"samedi": ((samedi+5760)/10000*100).toFixed(1),
				"deuxsoirs": (100.0).toFixed(1),
				"derniereminute": Math.max(pourcentageplace, pourcentagedate)
			});

		}else{
			res.status(500);
			res.send({"error": "problem to get stats"});
		}

	});

});

module.exports = router;
